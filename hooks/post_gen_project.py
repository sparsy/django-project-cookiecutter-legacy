# -*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function

import hashlib
import os
import random
import shutil

from cookiecutter.main import cookiecutter

# POST HOOK VARS INIT

# projects
project_directory = os.path.realpath(os.path.curdir)
project_name = '{{ cookiecutter.project_name }}'

# settings
settings_path = os.path.join(project_directory, project_name, 'settings')
local_path = os.path.join(settings_path, 'local.py')
with open(local_path) as f:
    local_py = f.read()

# django-rest-framework
drf_path = os.path.join(project_directory, project_name, 'drf')


# POST HOOK METHODS INIT

# Use the system PRNG if possible
try:
    random = random.SystemRandom()
    using_sysrandom = True
except NotImplementedError:
    # import warnings
    # warnings.warn('A secure pseudo-random number generator is not available '
    #               'on your system. Falling back to Mersenne Twister.')
    using_sysrandom = False


# Generate a SECRET_KEY that matches the Django standard
def get_random_string(
        length=50,
        allowed_chars='abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'):
    """
    Returns a securely generated random string.
    The default length of 12 with the a-z, A-Z, 0-9 character set returns
    a 71-bit value. log_2((26+26+10)^12) =~ 71 bits
    """
    if not using_sysrandom:
        # This is ugly, and a hack, but it makes things better than
        # the alternative of predictability. This re-seeds the PRNG
        # using a value that is hard for an attacker to predict, every
        # time a random string is required. This may change the
        # properties of the chosen random sequence slightly, but this
        # is better than absolute predictability.
        random.seed(
            hashlib.sha256(
                ("%s%s%s" % (
                    random.getstate(),
                    time.time(),
                    settings.SECRET_KEY)).encode('utf-8')
            ).digest())
    return ''.join(random.choice(allowed_chars) for i in range(length))


# SETTINGS TO DEV

# Create symlink of 'dev.py' as 'settings.py' for development
source = 'dev.py'
destination = os.path.join(settings_path, 'settings.py')
os.symlink(source, destination)


# GENERATE SECRET_KEY

# Replace "!!SECRET_KEY!!" with SECRET_KEY
SECRET_KEY = get_random_string()
local_py = local_py.replace('!!SECRET_KEY!!', SECRET_KEY)

# Write the results to the locals.py module
with open(local_path, 'w') as f:
    f.write(local_py)


# DJANGO REST FRAMEWORK
{% if cookiecutter.djangorestframework == "n" -%}
    # drf folder removed if django-rest-framework not selected
    shutil.rmtree(drf_path)
{% endif -%}


# PROJECT APP
{% if not cookiecutter.project_app_title == "None" -%}

# project app context
context = {
    "app_title": "{{cookiecutter.project_app_title|capitalize}}",
    "app_description": "{{cookiecutter.project_app_title}} of {{cookiecutter.project_title}}",

    # context from django-project-cookiecutter to django-app-cookiecutter
    "__django_image_cropping": "{{cookiecutter.django_image_cropping}}",
    "__djangorestframework": "{{cookiecutter.djangorestframework}}",
    "__django_modeltranslation": "{{cookiecutter.django_modeltranslation}}",
}


# project app is created
cookiecutter(
    "bb:sparsy/django-app-cookiecutter",
    no_input=True,
    extra_context=context,
    output_dir='apps/',
)
{% endif -%}


# POST HOOK FINAL PRINT
print("Remember to execute:")
print("")
print("mkvirtualenv {{ cookiecutter.project_name }}")
print("cd {{ cookiecutter.project_name }}")
print("pip install -r requirements.txt")
print("python manage.py migrate")
print("")
