# -*- coding: utf-8 -*-

{#- MACRO definition  #}
{%- macro project_name_camel_case() -%}
    {% for word in cookiecutter.project_name.split('-') %}{{ word | capitalize}}{% endfor %}
{%- endmacro %}
"""
{{ project_name_camel_case() }} Exceptions tree
+-- {{ project_name_camel_case() }}Exception
    +-- YourCustomException
"""

import logging


# GENERIC EXCEPTION
class {{ project_name_camel_case() }}Exception(Exception):
    """
    To import:
        from {{cookiecutter.project_name}} import exceptions

    To raise:
        raise exceptions.{{ project_name_camel_case() }}Exception('text of error')

    To capture:
        try:
            [code]
        except exceptions.{{ project_name_camel_case() }}Exception as e:
            [code]
    """
    msg = ''

    def __init__(self, msg=None, write_log=True, level='ERROR'):
        """
        :param msg: error text message
        :param write_log: if true a log is written in {{cookiecutter.log_path}}{{cookiecutter.project_name}}-exceptions.log
        """
        self.exception_logger = logging.getLogger('exceptions')
        self.log_level = level

        if msg:
            self.msg = msg
        if write_log:
            self.write_log(self.msg)

    def get_repr_str(self):
        repr_str = self.__class__.__name__ + u': '
        repr_str += u'{} {}'.format(self.log_level, self.msg)
        return repr_str

    def __str__(self):
        return repr(self.get_repr_str())

    def __repr__(self):
        return repr(self.get_repr_str())

    def write_log(self, msg):
        getattr(self.exception_logger, self.log_level.lower())(msg)


# CUSTOM EXCEPTIONS
class YourCustomException({{ project_name_camel_case() }}Exception):
    """
    To import:
        from {{cookiecutter.project_name}} import exceptions

    To raise:
        raise exceptions.YourCustomException('text of error')

    To capture:
        try:
            [code]
        except exceptions.YourCustomException as e:
            [code]
    """
    pass
