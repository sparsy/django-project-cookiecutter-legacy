# -*- coding: utf-8 -*-
{#- MACRO definition  #}
{%- macro project_name_camel_case() -%}
    {% for word in cookiecutter.project_name.split('-') %}{{ word | capitalize}}{% endfor %}
{%- endmacro -%}
{%- macro project_name_lower_case() -%}
    {{ cookiecutter.project_name | replace("-", "_") | lower }}
{%- endmacro %}
"""
{{ project_name_camel_case() }} APIExceptions tree
+-- APIException (DRF)
    +-- {{ project_name_camel_case() }}APIException
"""

import logging

from rest_framework.exceptions import APIException
from rest_framework.request import Request
from rest_framework.views import exception_handler


# API EXCEPTION HANDLER
def {{ project_name_lower_case() }}_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    # Now add the HTTP status code to the response.
    if response is not None:
        response.data['status_code'] = response.status_code

    return response


# GENERIC CUSTOM API EXCEPTION
class {{ project_name_camel_case() }}APIException(APIException):
    """
    To import:
        from {{ cookiecutter.project_name }}.drf import exceptions

    To raise:
        from rest_framework import status

        raise exceptions.{{ project_name_camel_case() }}APIException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            request=request,
            msg='text of error',
        )
    """

    def __init__(self, status_code, request, msg=None, write_log=True):
        """
        :param status_code: HTTP status code
        :param request: view's request
        :param msg: error description
        :param write_log: if true a log is written in {{cookiecutter.log_path}}{{cookiecutter.project_name}}-api_exceptions.log
        """
        self.logger = logging.getLogger('api_exceptions')
        self.status_code = status_code
        self.request = request
        if type(self.request) == Request:
            # if request is DRF-request then self.request = DJANGO-request
            self.request = self.request._request
        self.http_method = request.method

        super({{ project_name_camel_case() }}APIException, self).__init__(msg)
        if write_log:
            log_string = """{detail}\nurl: {url}\nmethod: {method}\nuser: {user}\ndata: {data}\n""".format(
                detail=self.detail,
                url=request.get_full_path(),
                method=getattr(request, 'method', ''),
                user=getattr(request, 'user', ''),
                data=getattr(request, 'data', ''),
            )
            self.logger.error(log_string)

    def get_repr_str(self):
        repr_str = u''
        if self.http_method:
            repr_str += '{} '.format(self.http_method.upper())
        repr_str += u'{} {}'.format(self.status_code, self.default_detail)
        return repr_str

    def __str__(self):
        return repr(self.get_repr_str())

    def __repr__(self):
        return repr(self.get_repr_str())
